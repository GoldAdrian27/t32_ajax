$("#getButton").on('click', del);

function del() {
  var id = $("#numberInput").val();
  $.ajax({
    url: "http://localhost:8181/api/products/" + id,
    method: "DELETE",
    dataType: "json",
    headers: {
      'Accept': 'application/json'
    },
    success: function() {
      console.log("eliminado correctamente");
    },
    error: function(error) {
      console.log(error);
    }
  });
}
