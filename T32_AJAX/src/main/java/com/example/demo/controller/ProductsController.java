package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.dto.Products;
import com.example.demo.service.ProductsServiceImpl;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProductsController {

	@Autowired
	ProductsServiceImpl productServiceImpl;
	
	@GetMapping("/products")
	public List<Products> listarProducts(){
		return productServiceImpl.listarProducts();
	}
	
//	@RequestMapping(value = "/products", method = RequestMethod.POST,
//	        consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
//	        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	@PostMapping("/products")
	public Products guardarProduct(@RequestBody Products product) {
		return productServiceImpl.guardarProduct(product);
	}
	
	@GetMapping("/products/{id}")
	public Products productXID(@PathVariable(name="id") int id) {
		Products product_xid = new Products();
		
		product_xid = productServiceImpl.productXID(id);
		
		return product_xid;
	}
	
	@PutMapping("/products/{id}")
	public Products actualizarProduct(@PathVariable(name="id")int id, @RequestBody Products product) {
		
		Products product_seleccionado = new Products();
		Products product_actualizado = new Products();
		
		product_seleccionado = productServiceImpl.productXID(id);
		
		product_seleccionado.setName(product.getName());
		product_seleccionado.setDetail(product.getDetail());
		product_seleccionado.setCreatedAt(product.getCreatedAt());
		product_seleccionado.setUpdatedAt(product.getUpdatedAt());
		
		product_actualizado = productServiceImpl.actualizarProduct(product_seleccionado);
		
		return product_actualizado;
	}
	
	@DeleteMapping("/products/{id}")
	public void eliminarDeartamento(@PathVariable(name="id")int id) {
		productServiceImpl.eliminarProduct(id);
	}
	
}
