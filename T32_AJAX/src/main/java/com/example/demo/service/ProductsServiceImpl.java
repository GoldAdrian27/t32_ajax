package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IProductsDAO;
import com.example.demo.dto.Products;

@Service
public class ProductsServiceImpl implements IProductsServiceImpl{
	
	@Autowired
	IProductsDAO iProductDAO;
	
	@Override
	public List<Products> listarProducts() {

		return iProductDAO.findAll();
	}

	@Override
	public Products guardarProduct(Products departamento) {

		return iProductDAO.save(departamento);
	}

	@Override
	public Products productXID(int id) {

		return iProductDAO.findById(id).get();
	}

	@Override
	public Products actualizarProduct(Products departamento) {

		return iProductDAO.save(departamento);
	}

	@Override
	public void eliminarProduct(int id) {

		iProductDAO.deleteById(id);
	}

}
