package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Products;


public interface IProductsServiceImpl {
	//Metodos del CRUD
	public List<Products> listarProducts(); //Listar All 
	
	public Products guardarProduct(Products product);	//Guarda un Articulo CREATE
	
	public Products productXID(int id); //Leer datos de un Articulo READ
	
	public Products actualizarProduct(Products product); //Actualiza datos del Articulo UPDATE
	
	public void eliminarProduct(int id);// Elimina el Articulo DELETE
}
